<?php
/**
 * Created by PhpStorm.
 * User: andriy
 * Date: 13.08.17
 * Time: 17:33
 */

namespace AppBundle\Entity;


class DeptManager
{
    private $fromDate;
    private $toDate;

    private $department;
    private $employee;

    public function __construct(Department $department, Employee $employee, \DateTime $fromDate, \DateTime $toDate)
    {
        $this->department = $department;
        $this->employee = $employee;
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
    }

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Department $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return mixed
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * @param mixed $fromDate
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;
    }

    /**
     * @return mixed
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * @param mixed $toDate
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;
    }
}