Symfony tutorial application
============================

A Symfony project created on August 12, 2017, 11:47 pm.

Maintainer Andriy Doroshenko mapleukraine@gmail.com

This project made for tutorial purposes and not optimized for performance or any kind of security issues, runs in development environment. It utilises built-in php web server.

As database engine it uses preconfigured docker MySQL image which will be downloaded on startup (~800 Mb). It contains [free distributed employee database](https://dev.mysql.com/doc/employee/en/) (about 400 M of data) with minimal changes.

### Prerequisites:

* PHP >= 5.4
* composer
* Docker
* your local port 13306 should be free
* your local port 8000 should be free

### Run project:

Before first start run ```composer install``` to download vendor libraries.

Since it  


Start project with:

```
./run.sh
```

Go to browser and surf to [http://localhost:8000](http://localhost:8000)

### Stop project:

** Press ```Ctrl+C``` to stop server, then run ```clear.sh``` which stops docker database container and delete it.
To fully remove database image run:

```
docker rmi mapleukraine/symtest-data:latest
```