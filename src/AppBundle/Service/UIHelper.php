<?php
/**
 * Created by PhpStorm.
 * User: andriy
 * Date: 13.08.17
 * Time: 15:20
 */

namespace AppBundle\Service;

use AppBundle\Entity\Department;
use AppBundle\Repository\DepartmentRepository;
use Doctrine\ORM\EntityManagerInterface;

class UIHelper
{
    /** @var DepartmentRepository */
    private $departmentRepository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->departmentRepository = $em->getRepository(Department::class);
    }

    public function getDepartments()
    {
        return $this->departmentRepository->findAllOrderedByName();
    }
}