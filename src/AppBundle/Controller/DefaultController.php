<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Entity\Department;
use AppBundle\Entity\DeptEmployee;

class DefaultController extends Controller
{
    const UNKNOWN_DEPARTMENT = 'unknown';
    const TAB_EMPLOYEES = 'employees';
    const TAB_MANAGERS = 'managers';

    /**
     * Example: /d001/1
     * @param Request $request
     * @param string $department_id
     * @param integer $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $department_id, $page = 1)
    {
        $session = $request->getSession();
        $tabToShow = $session->get('tab', self::TAB_EMPLOYEES);
        $requestedTab = $request->get('tab');
        if (!is_null($requestedTab) && $tabToShow !== $requestedTab) {
            $session->set('tab', $requestedTab);
            $tabToShow = $requestedTab;
        }

        $department = $this->getDoctrine()
            ->getRepository(Department::class)
            ->find($department_id);

        if (self::TAB_EMPLOYEES === $tabToShow) {
            $limit = 100;
            $paginator = $this->getDoctrine()
                ->getRepository(DeptEmployee::class)
                ->paginate($department, $page, $limit);
            $maxPages = ceil($paginator->count() / $limit);
            return $this->render('index_employees.html.twig', [
                'department' => $department,
                'parted' => $paginator,
                'page' => $page,
                'maxPages' => $maxPages,
                'tab' => $tabToShow,
                'smallStep' => 15,
                'bigStep' => 50,
            ]);

        } else {
            return $this->render('index_managers.html.twig', [
                'department' => $department,
                'tab' => $tabToShow,
            ]);
        }

    }

    /**
     * Example: /
     * @param Request $request
     * @return Response
     */
    public function emptyQueryAction(Request $request)
    {
        /** @var Department $firstDepartment */
        $firstDepartment = $this->getDoctrine()
            ->getRepository(Department::class)
            ->findFirst();
        if ($firstDepartment) {
            $queryParam = $firstDepartment->getDeptNo();
        } else {
            $queryParam = self::UNKNOWN_DEPARTMENT;
        }
        $url = $this->generateUrl('homepage', ['department_id' => $queryParam]);
        return $this->redirect($url, 301);
    }
}
