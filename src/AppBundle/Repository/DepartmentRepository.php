<?php
/**
 * Created by PhpStorm.
 * User: andriy
 * Date: 13.08.17
 * Time: 14:28
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class DepartmentRepository extends EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->getQuery()->getResult();
    }

    public function findFirst()
    {
        $resultSet = $this->getQuery()->setFirstResult(1)->getResult();
        if (count($resultSet) > 0) {
            return $resultSet[0];
        } else {
            return null;
        }
    }

    /**
     * Creates a Query object to retrieve departments list.
     * @return \Doctrine\ORM\Query
     */
    private function getQuery()
    {
        return $this->getEntityManager()
            ->createQuery('select d from AppBundle:Department d order by d.deptName');
    }
}