<?php
/**
 * Created by PhpStorm.
 * User: andriy
 * Date: 13.08.17
 * Time: 13:56
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;

class Department
{
    private $deptNo;
    private $deptName;
    private $managers;
    private $employees;

    public function __construct()
    {
        $this->managers = new ArrayCollection();
        $this->employees = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @param ArrayCollection $employees
     */
    public function setEmployees($employees)
    {
        $this->employees = $employees;
    }

    /**
     * @return mixed
     */
    public function getManagers()
    {
        return $this->managers;
    }

    /**
     * @param mixed $manager
     */
    public function setManager($manager)
    {
        //todo
    }

    /**
     * @return mixed
     */
    public function getDeptNo()
    {
        return $this->deptNo;
    }

    /**
     * @param mixed $deptNo
     */
    public function setDeptNo($deptNo)
    {
        $this->deptNo = $deptNo;
    }

    /**
     * @return mixed
     */
    public function getDeptName()
    {
        return $this->deptName;
    }

    /**
     * @param mixed $deptName
     */
    public function setDeptName($deptName)
    {
        $this->deptName = $deptName;
    }


}