<?php
/**
 * Created by PhpStorm.
 * User: andriy
 * Date: 13.08.17
 * Time: 14:28
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Department;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DeptEmployeeRepository extends EntityRepository
{
    public function getDeptEmployeesQuery(Department $department, $searchLowerCase = null)
    {
        $query = $this->getEntityManager()
            ->createQuery(implode(' ', [
                'select e, a from AppBundle:DeptEmployee e ',
                'join e.employee a ',
                'where e.department = :department ',
                is_null($searchLowerCase) ? '' : 'and LOWER(a.lastName) like :search',
                'order by a.lastName']))
            ->setParameter('department', $department);
        if (!is_null($searchLowerCase)) {
            $query->setParameter('search', $searchLowerCase);
        }
        return $query;
    }

    public function paginate(Department $department, $page = 1, $limit = 10, $searchLowerCase = null)
    {
        $paginator = new Paginator($this->getDeptEmployeesQuery($department, $searchLowerCase), false);
        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit);
        return $paginator;
    }
}