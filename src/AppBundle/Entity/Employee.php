<?php
/**
 * Created by PhpStorm.
 * User: andriy
 * Date: 13.08.17
 * Time: 11:37
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;

class Employee
{
    const GENDER_MALE = 'M';
    const GENDER_FEMALE = 'F';

    private $empNo;
    private $birthDate;
    private $firstName;
    private $lastName;
    private $gender;
    private $hireDate;
    private $deptManaged;
    private $deptParted;

    public function __construct()
    {
        $this->deptManaged = new ArrayCollection();
        $this->deptParted = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getDeptParted()
    {
        return $this->deptParted;
    }

    /**
     * @param ArrayCollection $deptParted
     */
    public function setDeptParted($deptParted)
    {
        $this->deptParted = $deptParted;
    }

    /**
     * @return ArrayCollection
     */
    public function getDeptManaged()
    {
        return $this->deptManaged;
    }

    /**
     * @param ArrayCollection $deptManaged
     */
    public function setDeptManaged($deptManaged)
    {
        $this->deptManaged = $deptManaged;
    }

    /**
     * @return mixed
     */
    public function getEmpNo()
    {
        return $this->empNo;
    }

    /**
     * @param mixed $empNo
     */
    public function setEmpNo($empNo)
    {
        $this->empNo = $empNo;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $gender = strtoupper($gender);
        if (!in_array($gender, [self::GENDER_MALE, self::GENDER_FEMALE])) {
            throw new \InvalidArgumentException('Invalid gender');
        }
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getHireDate()
    {
        return $this->hireDate;
    }

    /**
     * @param mixed $hireDate
     */
    public function setHireDate($hireDate)
    {
        $this->hireDate = $hireDate;
    }


}