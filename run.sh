#!/usr/bin/env bash
docker ps | awk '{print $NF}' | grep -w "symtest-data" && bash clear.sh
docker run --name symtest-data \
    -p 13306:3306 \
    -d mapleukraine/symtest-data:latest
php app/console server:run